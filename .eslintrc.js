module.exports = {
  parser: 'babel-eslint',
  parserOptions: {
    allowImportExportEverywhere: true,
  },
  plugins: ['meteor'],
  extends: ['airbnb', 'plugin:meteor/recommended'],
  settings: {
    'import/resolver': 'meteor',
    'import/core-modules': ['meteor/meteor', 'meteor/mongo', 'meteor/check'],
  },
  rules: {
    'no-undef': [2],
    'import/extensions': [0],
    'import/prefer-default-export': [0],
  },
  env: {
    meteor: true,
    node: true,
    browser: true,
  },
};
