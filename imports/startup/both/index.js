// Import modules used by both client and server through a single index entry point
// e.g. useraccounts configuration file.

// UserAccounts config
AccountsTemplates.configure({
  defaultLayoutType: 'blaze', // Optional, the default is 'blaze'
  defaultTemplate: 'myCustomFullPageAtForm',
  defaultLayout: 'App_body',
  defaultLayoutRegions: {
    nav: 'App_nav',
    sidebar: 'App_sidebar',
    footer: 'App_footer',
  },
  defaultContentRegion: 'main',
});

const pwd = AccountsTemplates.removeField('password');
AccountsTemplates.removeField('email');
AccountsTemplates.addFields([
  {
    _id: 'username',
    type: 'text',
    displayName: 'username',
    required: true,
    minLength: 5,
  },
  {
    _id: 'email',
    type: 'email',
    required: true,
    displayName: 'email',
    re: /.+@(.+){2,}\.(.+){2,}/,
    errStr: 'Invalid email',
  },
  pwd,
]);
