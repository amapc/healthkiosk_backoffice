// Register your apis here

import '../../api/kiosks/methods.js';
import '../../api/kiosks/server/publications.js';

import '../../api/patients/methods.js';
import '../../api/patients/server/publications.js';

import '../../api/users/users.js';
import '../../api/users/server/publications.js';

import '../../api/chatrooms/methods.js';
import '../../api/chatrooms/server/publications.js';

import '../../api/users/users.js';
