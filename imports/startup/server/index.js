// Import server startup through a single index entry point

import './fixtures.js';
import './register-api.js';
import { Random } from 'meteor/random';
import { Chatrooms } from '../../api/chatrooms/chatrooms.js';
import { Patients } from '../../api/patients/patients.js';
import { Kiosks } from '../../api/kiosks/kiosks.js';
import { Users } from '../../api/users/users.js';
import { Restivus } from 'meteor/nimble:restivus';

// /Browser Policy////
// BrowserPolicy.content.allowOriginForAll('https://cdnjs.cloudflare.com');
// BrowserPolicy.content.allowOriginForAll('https://maxcdn.bootstrapcdn.com');
// BrowserPolicy.content.allowOriginForAll('https://hkbo.site');
// BrowserPolicy.content.allowFontDataUrl();
// BrowserPolicy.content.allowFrameOrigin('https://hkbo.site');
// BrowserPolicy.framing.restrictToOrigin('https://hkbo.site');

// /REST///
const Api = new Restivus({
  useDefaultAuth: true,
  prettyJson: true,
});

Api.addRoute('add-chatroom', {
  get() {
    const id = Random.id();
    Chatrooms.insert({
      name: id,
      createdAt: new Date(),
    });
    return {
      status: 'success',
      data: {
        name: id,
      },
    };
  },
});

// Users endpoint

Api.addCollection(Meteor.users, {
  excludedEndpoints: ['getAll', 'put', 'delete', 'patch'],
  routeOptions: {
    authRequired: false,
  },
  endpoints: {
    post: {
      authRequired: false,
    },
  },
});

Api.addRoute('request-assistant', {
  post() {
    const params = this.bodyParams;
    const screenStatus = params.screenStatus;
    const language = params.language;
    const specialty = params.specialty;

    // const practitioners = Meteor.users.findOne({
    //   'status.online': true,
    //   'profile.role.callStatus': 'Available',
    // });
    // // if language != '' // if speciality != ''...
    // console.log(practitioners);
    const id = Meteor.call('chatrooms.getAssistant', language, screenStatus, specialty);
    return {
      status: 'success',
      data: {
        id,
        // practitioners,
        // params,
      },
    };
  },
});

Api.addRoute('send-measurements/:_id', {
  put() {
    const id = this.urlParams._id;
    Meteor.call('patients.addExams', id, this.bodyParams.examType, this.bodyParams.exam);

    return {
      status: 'success',
      data: {
        id,
      },
    };
  },
});

Api.addRoute('create-room', {
  // SEND created roomID
  post() {
    const patientID = this.bodyParams.patientID;
    const kioskID = this.bodyParams.kioskID;
    const assistantID = this.bodyParams.assistantID;
    const roomID = Meteor.call('chatrooms.create', patientID, kioskID, assistantID);
    return {
      statusCode: 201,
      status: 'success',
      data: { roomID },
    };
  },
});

/* Api.addRoute('add-kiosk', {
  post() {
    return {
      status: 'success',
      data: {},
    };
  },
}); */
Api.addRoute('add-kiosk', {
  post() {
    return {
      status: 'success',
      data: {},
    };
  },
});

Api.addCollection(Chatrooms);
Api.addCollection(Patients);
Api.addCollection(Kiosks);
