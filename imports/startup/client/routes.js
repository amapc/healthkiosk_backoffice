import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';

// Import needed templates
import '../../ui/layouts/body/body.js';
import '../../ui/layouts/nav/nav.js';
import '../../ui/layouts/footer/footer.js';
import '../../ui/layouts/sidebar/sidebar.js';

import '../../ui/pages/dashboard/dashboard.js';
import '../../ui/pages/profile/profile.js';
import '../../ui/pages/not-found/not-found.js';
import '../../ui/pages/videocall/videocall.js';

// routes.js

AccountsTemplates.configureRoute('signIn', {
  layoutType: 'blaze',
  name: 'signin',
  path: '/',
  template: 'fullPageAtForm',
  layoutTemplate: 'App_body',
  layoutRegions: {
    nav: 'App_nav',
    //footer: 'App_footer',
  },
  contentRegion: 'main',
});

// Set up all routes in the app
FlowRouter.route('/dashboard', {
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  name: 'App.dashboard',
  action() {
    BlazeLayout.render('App_body', {
      main: 'App_dashboard',
      nav: 'App_nav',
      sidebar: 'App_sidebar',
      //footer: 'App_footer',
    });
  },
});
FlowRouter.route('/profile', {
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  name: 'App.profile',
  action() {
    BlazeLayout.render('App_body', {
      main: 'App_profile',
      nav: 'App_nav',
      //footer: 'App_footer',
    });
  },
});
FlowRouter.route('/videocall', {
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  name: 'App.videocall',
  action() {
    BlazeLayout.render('App_body', {
      main: 'App_videocall',
      nav: 'App_nav',
      //footer: 'App_footer',
    });
  },
});

FlowRouter.route('/logout', {
  name: 'logout',
  action() {
    AccountsTemplates.logout();
    FlowRouter.go('/asd');
  },
});

FlowRouter.notFound = {
  action() {
    BlazeLayout.render('App_body', { main: 'App_notFound' });
  },
};
