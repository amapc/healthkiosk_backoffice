import { Meteor } from 'meteor/meteor';

Meteor.publish('userStatus', () =>
  Meteor.users.find(
    {
      'status.online': true,
    },
    {
      fields: {
        profile: 1,
        status: 1,
        username: 1,
      },
    },
  ));
