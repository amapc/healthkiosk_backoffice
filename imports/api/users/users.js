import SimpleSchema from 'simpl-schema';
SimpleSchema.extendOptions(['autoform']);

const Schema = {};

Schema.Role = new SimpleSchema({
  organization: {
    optional: true,
    type: String, // (Organization)
  },
  specialty: {
    optional: true,
    type: String,
  },
  location: {
    optional: true,
    type: String,
  }, // (Location) ],
  callStatus: {
    optional: true,
    type: String,
    allowedValues: ['Available', 'Busy', 'Away'],
    autoValue() {
      if (this.isInsert) {
        return 'Available';
      }
    },
  },
  assistantType: {
    optional: true,
    allowedValues: ['TechSupport', 'HealthProfessional'],
    type: String,
    autoform: {
      type: 'select',
      options() {
        return [
          { label: 'Tech Support', value: 'TechSupport' },
          { label: 'Health Professional', value: 'HealthProfessional' },
        ];
      },
    },
  },
});

Schema.UserCountry = new SimpleSchema({
  name: {
    type: String,
  },
  code: {
    type: String,
    regEx: /^[A-Z]{2}$/,
  },
});

Schema.UserProfile = new SimpleSchema({
  firstName: {
    type: String,
    optional: true,
  },
  lastName: {
    type: String,
    optional: true,
  },
  birthDate: {
    type: Date,
    optional: true,
  },
  gender: {
    type: String,
    allowedValues: ['Male', 'Female'],
    optional: true,
  },
  country: {
    type: Schema.UserCountry,
    optional: true,
  },
  telecom: {
    type: Number,
    optional: true,
  },
  role: {
    type: Schema.Role,
    optional: true,
  },
  language: {
    optional: true,
    type: String,
  },
});

Schema.User = new SimpleSchema({
  username: {
    type: String,
    // For accounts-password, either emails or username is required, but not both. It is OK to make this
    // optional here because the accounts-password package does its own validation.
    // Third-party login packages may not require either. Adjust this schema as necessary for your usage.
    optional: true,
  },
  emails: {
    type: Array,
    // For accounts-password, either emails or username is required, but not both. It is OK to make this
    // optional here because the accounts-password package does its own validation.
    // Third-party login packages may not require either. Adjust this schema as necessary for your usage.
    optional: true,
  },
  'emails.$': {
    type: Object,
  },
  'emails.$.address': {
    type: String,
    regEx: SimpleSchema.RegEx.Email,
  },
  'emails.$.verified': {
    type: Boolean,
  },

  createdAt: {
    type: Date,
  },
  profile: {
    type: Schema.UserProfile,
    optional: true,
  },
  // Make sure this services field is in your schema if you're using any of the accounts packages
  services: {
    type: Object,
    optional: true,
    blackbox: true,
  },
  // Add `roles` to your schema if you use the meteor-roles package.
  // Option 1: Object type
  // If you specify that type as Object, you must also specify the
  // `Roles.GLOBAL_GROUP` group whenever you add a user to a role.
  // Example:
  // Roles.addUsersToRoles(userId, ["admin"], Roles.GLOBAL_GROUP);
  // You can't mix and match adding with and without a group since
  // you will fail validation in some cases.
  roles: {
    type: Object,
    optional: true,
    blackbox: true,
  },

  // In order to avoid an 'Exception in setInterval callback' from Meteor
  heartbeat: {
    type: Date,
    optional: true,
  },
  // Status module information  https://github.com/mizzao/meteor-user-status#basic-usage---online-state
  // Public and sent to any client

  status: {
    type: Object,
    optional: true,
    blackbox: true,
  },
});

Meteor.users.attachSchema(Schema.User);
Meteor.users.allow({
  insert: () => true,
  update: () => true,
  remove: () => true,
});
