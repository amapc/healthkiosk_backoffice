// Definition of the healthkiosks collection

import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

export const Kiosks = new Mongo.Collection('kiosks');

const Schema = {};

Schema.Kiosks = new SimpleSchema({
  NAME: {
    type: String,
    optional: true,
  },
  AUTHENTICATION: {
    type: Array,
  },
  LANGUAGES: {
    type: Array,
    optional: true,
  },
  'LANGUAGES.$': {
    type: String,
  },
  USER: {
    type: Array,
  },
  'USER.$': {
    type: String,
  },
  INACTIVITY: {
    type: Object,
  },
  'INACTIVITY.TIME': {
    type: Number,
  },
  'INACTIVITY.STATE': {
    type: String,
  },
  RESET: {
    type: Object,
  },
  'RESET.TIME': {
    type: Number,
  },
  'RESET.STATE': {
    type: String,
  },
  QRCODE: {
    type: String,
  },
  PRINTER: {
    type: String,
  },
  VOICE_INSTRUCTIONS: {
    type: String,
  },
  // change this structure??
  EXAMS: {
    type: Object,
    optional: true,
  },
  'EXAMS.nonin3230': {
    type: Object,
    optional: true,
  },
  'EXAMS.nonin3230.mac_address': {
    type: String,
  },
  'EXAMS.nonin3230.requirements': {
    type: Array,
  },
  'EXAMS.nonin3230.requirements.$': {
    type: String,
  },
  'EXAMS.nonin3230.parameters': {
    type: Object,
  },
  'EXAMS.nonin3230.parameters.MDC_PULS_OXIM_SAT_O2': {
    type: Object,
  },
  'EXAMS.nonin3230.parameters.MDC_PULS_OXIM_SAT_O2.limits': {
    type: Array,
  },
  'EXAMS.nonin3230.parameters.MDC_PULS_OXIM_SAT_O2.limits.$': {
    type: String,
  },
  'EXAMS.nonin3230.parameters.MDC_PULS_OXIM_SAT_O2.default': {
    type: Array,
  },
  'EXAMS.nonin3230.parameters.MDC_PULS_OXIM_SAT_O2.default.$': {
    type: String,
  },
  'EXAMS.nonin3230.parameters.MDC_PULS_OXIM_SAT_O2.chart': {
    type: Boolean,
  },
  'EXAMS.nonin3230.parameters.MDC_PULS_OXIM_PULS_RATE': {
    type: Object,
  },
  'EXAMS.nonin3230.parameters.MDC_PULS_OXIM_PULS_RATE.limits': {
    type: Array,
  },
  'EXAMS.nonin3230.parameters.MDC_PULS_OXIM_PULS_RATE.limits.$': {
    type: String,
  },
  'EXAMS.nonin3230.parameters.MDC_PULS_OXIM_PULS_RATE.default': {
    type: Array,
  },
  'EXAMS.nonin3230.parameters.MDC_PULS_OXIM_PULS_RATE.default.$': {
    type: String,
  },
  'EXAMS.nonin3230.parameters.MDC_PULS_OXIM_PULS_RATE.chart': {
    type: Boolean,
  },
  'EXAMS.ua-767pbt-ci': {
    type: Object,
    optional: true,
  },
  'EXAMS.ua-767pbt-ci.mac_address': {
    type: String,
  },
  'EXAMS.ua-767pbt-ci.requirements': {
    type: Array,
  },
  'EXAMS.ua-767pbt-ci.requirements.$': {
    type: String,
  },
  'EXAMS.ua-767pbt-ci.parameters': {
    type: Object,
  },
  'EXAMS.ua-767pbt-ci.parameters.MDC_PRESS_BLD_NONINV_SYS': {
    type: Object,
  },
  'EXAMS.ua-767pbt-ci.parameters.MDC_PRESS_BLD_NONINV_SYS.limits': {
    type: Array,
  },
  'EXAMS.ua-767pbt-ci.parameters.MDC_PRESS_BLD_NONINV_SYS.limits.$': {
    type: String,
  },
  'EXAMS.ua-767pbt-ci.parameters.MDC_PRESS_BLD_NONINV_SYS.default': {
    type: Array,
  },
  'EXAMS.ua-767pbt-ci.parameters.MDC_PRESS_BLD_NONINV_SYS.default.$': {
    type: String,
  },
  'EXAMS.ua-767pbt-ci.parameters.MDC_PRESS_BLD_NONINV_SYS.chart': {
    type: Boolean,
  },
  'EXAMS.ua-767pbt-ci.parameters.MDC_PRESS_BLD_NONINV_DIA': {
    type: Object,
  },
  'EXAMS.ua-767pbt-ci.parameters.MDC_PRESS_BLD_NONINV_DIA.limits': {
    type: Array,
  },
  'EXAMS.ua-767pbt-ci.parameters.MDC_PRESS_BLD_NONINV_DIA.limits.$': {
    type: String,
  },
  'EXAMS.ua-767pbt-ci.parameters.MDC_PRESS_BLD_NONINV_DIA.default': {
    type: Array,
  },
  'EXAMS.ua-767pbt-ci.parameters.MDC_PRESS_BLD_NONINV_DIA.default.$': {
    type: String,
  },
  'EXAMS.ua-767pbt-ci.parameters.MDC_PRESS_BLD_NONINV_DIA.chart': {
    type: Boolean,
  },
  'EXAMS.ua-767pbt-ci.parameters.MDC_PULS_RATE_NON_INV': {
    type: Object,
  },
  'EXAMS.ua-767pbt-ci.parameters.MDC_PULS_RATE_NON_INV.limits': {
    type: Array,
  },
  'EXAMS.ua-767pbt-ci.parameters.MDC_PULS_RATE_NON_INV.limits.$': {
    type: String,
  },
  'EXAMS.ua-767pbt-ci.parameters.MDC_PULS_RATE_NON_INV.default': {
    type: Array,
  },
  'EXAMS.ua-767pbt-ci.parameters.MDC_PULS_RATE_NON_INV.default.$': {
    type: String,
  },
  'EXAMS.ua-767pbt-ci.parameters.MDC_PULS_RATE_NON_INV.chart': {
    type: Boolean,
  },

  'EXAMS.uc-351pbt-ci': {
    type: Object,
    optional: true,
  },
  'EXAMS.uc-351pbt-ci.mac_address': {
    type: String,
  },
  'EXAMS.uc-351pbt-ci.requirements': {
    type: Array,
  },
  'EXAMS.uc-351pbt-ci.requirements.$': {
    type: String,
  },
  'EXAMS.uc-351pbt-ci.parameters': {
    type: Object,
  },
  'EXAMS.uc-351pbt-ci.parameters.MDC_MASS_BODY_ACTUAL': {
    type: Object,
  },
  'EXAMS.uc-351pbt-ci.parameters.MDC_MASS_BODY_ACTUAL.limits': {
    type: Array,
  },
  'EXAMS.uc-351pbt-ci.parameters.MDC_MASS_BODY_ACTUAL.limits.$': {
    type: String,
  },
  'EXAMS.uc-351pbt-ci.parameters.MDC_MASS_BODY_ACTUAL.default': {
    type: Array,
  },
  'EXAMS.uc-351pbt-ci.parameters.MDC_MASS_BODY_ACTUAL.default.$': {
    type: String,
  },
  'EXAMS.uc-351pbt-ci.parameters.MDC_MASS_BODY_ACTUAL.chart': {
    type: Boolean,
  },

  // Force value to be current date (on server) upon insert
  // and prevent updates thereafter.
  createdAt: {
    type: Date,
    autoValue() {
      if (this.isInsert) {
        return new Date();
      } else if (this.isUpsert) {
        return { $setOnInsert: new Date() };
      }
      this.unset(); // Prevent user from supplying their own value
    },
  },
  // Force value to be current date (on server) upon update
  // and don't allow it to be set upon insert.
  updatedAt: {
    type: Date,
    autoValue() {
      if (this.isUpdate) {
        return new Date();
      }
    },
    denyInsert: true,
    optional: true,
  },
});

Kiosks.attachSchema(Schema.Kiosks);
