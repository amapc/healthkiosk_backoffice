// All kiosks-related publications

import { Meteor } from 'meteor/meteor';
import { Kiosks } from '../kiosks.js';

Meteor.publish('kiosks.all', () => Kiosks.find());
