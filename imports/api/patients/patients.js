// Definition of the patients collection

import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

export const Patients = new Mongo.Collection('patients');

const Schema = {};
Schema.Values = new SimpleSchema({
  value: {
    type: Number,
  },
  device: {
    type: String,
  },
  date: {
    type: String,
  },
});

Schema.Patients = new SimpleSchema({
  _id: {
    type: String,
  },
  /* ID: {
    type: String,
  },*/
  AGE: {
    type: Number,
  },
  GENDER: {
    type: String,
  },
  HEIGHT: {
    type: Number,
  },
  RESULTS: {
    type: Object,
  },
  'RESULTS.MDC_PULS_OXIM_SAT_O2': {
    type: Array,
  },
  'RESULTS.MDC_PULS_OXIM_SAT_O2.$': {
    type: Schema.Values,
  },
  'RESULTS.MDC_PULS_OXIM_PULS_RATE': {
    type: Array,
  },
  'RESULTS.MDC_PULS_OXIM_PULS_RATE.$': {
    type: Schema.Values,
  },
  'RESULTS.MDC_PRESS_BLD_NONINV_SYS': {
    type: Array,
  },
  'RESULTS.MDC_PRESS_BLD_NONINV_SYS.$': {
    type: Schema.Values,
  },
  'RESULTS.MDC_PRESS_BLD_NONINV_DIA': {
    type: Array,
  },
  'RESULTS.MDC_PRESS_BLD_NONINV_DIA.$': {
    type: Schema.Values,
  },
  'RESULTS.MDC_PULS_RATE_NON_INV': {
    type: Array,
  },
  'RESULTS.MDC_PULS_RATE_NON_INV.$': {
    type: Schema.Values,
  },
  'RESULTS.MDC_MASS_BODY_ACTUAL': {
    type: Array,
  },
  'RESULTS.MDC_MASS_BODY_ACTUAL.$': {
    type: Schema.Values,
  },
  LANGUAGE: {
    type: Object,
  },
  'LANGUAGE.FULL': {
    type: String,
  },
  'LANGUAGE.SMALL': {
    type: String,
  },
});

Patients.attachSchema(Schema.Patients);
