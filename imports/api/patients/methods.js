// Methods related to links

import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { Patients } from './patients.js';

Meteor.methods({
  'patients.addExams': function (_id, examType, exam) {
    check(_id, String);
    check(exam, Object);
    check(examType, String);
    const field = `RESULTS.${examType}`;
    check(field, String);
    // console.log(field);

    return Patients.update(_id, { $push: { [field]: exam } });
  },
});
