import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

export const Chatrooms = new Mongo.Collection('chatrooms');

const Schema = {};

Schema.Chatrooms = new SimpleSchema({
  name: {
    type: String,
    label: 'Name',
    max: 64,
    optional: true,
  },
  assistants: {
    type: Array,
    optional: true,
  },
  // waiting, finished, onprogress
  status: {
    type: String,
  },
  patientID: {
    type: String,
  },
  assistantID: {
    type: String,
  },
  kioskID: {
    type: String,
  },
  'assistants.$': {
    type: Object,
  },
  'assistants.$.id': {
    type: String,
  },
  'assistants.$.username': {
    type: String,
    optional: true,
  },
  specialty: {
    optional: true,
    type: String,
  },
  language: {
    optional: true,
    type: String,
  },
  // Force value to be current date (on server) upon insert
  // and prevent updates thereafter.
  createdAt: {
    type: Date,
    autoValue() {
      if (this.isInsert) {
        return new Date();
      } else if (this.isUpsert) {
        return { $setOnInsert: new Date() };
      }
      this.unset(); // Prevent user from supplying their own value
    },
  },
  // Force value to be current date (on server) upon update
  // and don't allow it to be set upon insert.
  updatedAt: {
    type: Date,
    autoValue() {
      if (this.isUpdate) {
        return new Date();
      }
    },
    denyInsert: true,
    optional: true,
  },
});

Chatrooms.attachSchema(Schema.Chatrooms);
