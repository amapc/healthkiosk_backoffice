// Methods related to chatrooms

import { Meteor } from 'meteor/meteor';
import { Chatrooms } from './chatrooms.js';
import { Patients } from '../patients/patients.js';
import { check } from 'meteor/check';

Meteor.methods({
  'chatrooms.updateStatus': function (roomID, roomStatus) {
    check(roomID, String);
    check(roomStatus, String);
    Chatrooms.update(
      { _id: roomID },
      {
        $set: {
          status: roomStatus,
        },
      },
    );
  },

  'chatrooms.create': function (patientID, kioskID, assistantID) {
    check(patientID, String);
    check(kioskID, String);
    check(assistantID, String);

    // get patient language
    // const currentPatient = Patients.findOne({ _id: patientID }, { LANGUAGE: 1 });
    // const language = currentPatient.LANGUAGE.SMALL;
    // get patient speciality
    // const specialty = 'none';
    // insert
    const roomID = Chatrooms.insert({
      patientID,
      kioskID,
      assistantID,
      status: 'waiting',
    });
    return roomID;
  },
  'chatrooms.getAssistant': function (language, screenStatus, specialty) {
    check(language, String);
    check(screenStatus, String);
    check(specialty, String);

    let assistantType = '';
    if (screenStatus === 'ThankYou_Screen') {
      assistantType = 'HealthProfessional';
    } else {
      assistantType = 'TechSupport';
    }
    const assistant = Meteor.users.findOne({
      'status.online': true,
      'profile.role.callStatus': 'Available',
      'profile.role.assistantType': assistantType,
      'profile.language': language,
      'profile.role.specialty': specialty,
    });
    if (!assistant) {
      return 'Assistant not available';
    }
    return assistant._id;
  },
});
