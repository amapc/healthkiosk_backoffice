import { Meteor } from 'meteor/meteor';
import { Chatrooms } from '../chatrooms.js';

Meteor.publish('chatrooms.all', () => Chatrooms.find());

// Meteor.publish('chatrooms.assistants', () => Chatrooms.find({}));
