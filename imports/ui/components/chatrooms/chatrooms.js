import { Chatrooms } from '/imports/api/chatrooms/chatrooms.js';
import { Patients } from '/imports/api/patients/patients.js';
import { Session } from 'meteor/session';
import { Meteor } from 'meteor/meteor';
import './chatrooms.html';

Template.chatrooms.onCreated(() => {
  Meteor.subscribe('chatrooms.all');
  Meteor.subscribe('patients.all');
  Session.set('videoURL', 'https://hkbo.site');
  Session.set('currentPatient', '');
  Session.set('roomID', '');
  const cursor = Chatrooms.find({
    status: 'waiting',
    assistantID: Meteor.userId(),
  });
  let count = 0;
  const handle = cursor.observeChanges({
    added(status, assistantID) {
      // sAlert.info('Your message');
      Bert.alert({
        title: 'New room',
        message: 'New room waiting for assistant',
        type: 'info',
        style: 'fixed-bottom',
        icon: 'fa-heartbeat',
      });
      count += 1;
      console.log(`new waiting room ${count}.`);
    },
  });
  // 1hour timeout 1000 = 1second
  setTimeout(() => handle.stop(), 3600000);
});

Template.chatrooms.helpers({
  settings() {
    return {
      fields: [
        { key: 'AGE', label: 'Age' },
        { key: 'HEIGHT', label: 'Height' },
        { key: 'LANGUAGE.SMALL', label: 'Language' },
        { key: 'RESULTS.MDC_PULS_OXIM_SAT_O2.0.value', label: 'SAT O2' },
        { key: 'RESULTS.MDC_PULS_OXIM_PULS_RATE.0.value', label: 'PULS RATE' },
        { key: 'RESULTS.MDC_PRESS_BLD_NONINV_DIA.0.value', label: 'NONINV DIA' },
        { key: 'RESULTS.MDC_PULS_RATE_NON_INV.0.value', label: 'PULS RATE NON INV' },
        { key: 'RESULTS.MDC_MASS_BODY_ACTUAL.0.value', label: 'BODY MASS' },

        //outros campos
      ],
    };
  },
  getPatient() {
    const patientID = Session.get('currentPatient');
    if (patientID) {
      return Patients.find({ _id: patientID });
    }
    return Patients.find({});
  },
  frameURL() {
    return Session.get('videoURL');
  },
  chatroomStatus() {
    return this.status;
  },
  chatrooms() {
    return Chatrooms.find({});
  },
  id() {
    return this._id;
  },
  chatroomsActive() {
    return Chatrooms.find({
      occupants: { $exists: true },
      $where: 'this.occupants.length>0',
    });
  },
  chatroomsWaitingLanguage() {
    return Chatrooms.find({
      status: 'waiting',
      language: Meteor.user().profile.role.language,
    });
  },
  chatroomsWaitingLanguageSpecialty() {
    // hack race condition...
    if (Meteor.user()) {
      return Chatrooms.find({
        status: 'waiting',
        specialty: Meteor.user().profile.role.specialty,
        language: Meteor.user().profile.role.language,
      });
    }
  },
  // display chatroom answer/reject button
  chatroomAssistant() {
    if (Meteor.user()) {
      return Chatrooms.findOne({
        status: 'waiting',
        assistantID: Meteor.userId(),
      });
    }
  },
  // display only chatrooms User can assist
  // get patientID (language,...)

  chatroomsAvailable() {
    return Chatrooms.find({
      status: 'waiting',
    });
  },
});

Template.chatrooms.events({
  // mostrar iframe com url jquery
  // load dos resultados
  'click .room': function (event) {
    event.preventDefault();
    console.log(`roomID ${event.target.value}`);
    const roomID = event.target.value;
    Session.set('roomID', roomID);
    // const roomID = Session.get('roomID');
    // set room status onprogress
    Meteor.call('chatrooms.updateStatus', roomID, 'onprogress');
    // set assitant callStatus
    Meteor.users.update(Meteor.userId(), { $set: { 'profile.role.callStatus': 'Busy' } });

    const baseurl = 'https://hkbo.site/call/';
    const url = baseurl + roomID;
    console.log(url);
    const patient = Chatrooms.findOne({ _id: roomID }, { patientID: 1, _id: 0 });
    const value = patient.patientID;
    Session.set('currentPatient', value);
    // show results window
    $('#results_div').show();
    Session.set('videoURL', url);

    // window.open(url);
  },
  'click .exitroom': function (event) {
    event.preventDefault();
    const roomID = Session.get('roomID');
    console.log(`exit room: ${roomID}`);
    // set room status onprogress
    Meteor.call('chatrooms.updateStatus', roomID, 'finished');
    // set assitant callStatus
    Meteor.users.update(Meteor.userId(), { $set: { 'profile.role.callStatus': 'Available' } });
    // show results window
    $('#results_div').hide();
    Session.set('videoURL', 'https://hkbo.site/');
  },

  'submit .insert-user': function (event) {
    event.preventDefault();
    const target = event.target;
    const username = target.username;
    const id = target.id;

    Meteor.call('chatrooms.insertUser', username.value, id.value, (error) => {
      if (error) {
        console.log(error.error);
      } else {
        username.value = '';
        id.value = '';
      }
    });
  },
});
