import './account.html';

import { Meteor } from 'meteor/meteor';
import { Schema } from '/imports/api/users/users.js';

Template.account.onCreated(() => {
  console.log(Meteor.userId());
});

Template.account.helpers({
  getId() {
    return Meteor.userId();
  },
  getSchema() {
    return Schema;
  },
});
