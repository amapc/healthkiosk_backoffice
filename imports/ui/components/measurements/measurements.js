import { Patients } from '/imports/api/patients/patients.js';
import { Meteor } from 'meteor/meteor';
import './measurements.html';

Template.measurements.onCreated(() => {
  Meteor.subscribe('patients.all');
});

Template.measurements.helpers({
  getPatientRecord(patientID) {
    return Patients.find({ _id: patientID }, { measurements: 1 });
  },
  getAllPatientRecords() {
    return Patients.find({}, { measurements: 1 });
  },
});
