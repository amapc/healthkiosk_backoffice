import { Meteor } from 'meteor/meteor';

import './status.html';

Template.status.onCreated(() => {
  Meteor.subscribe('userStatus');
});

Template.status.helpers({
  usersOnline() {
    return Meteor.users.find({ 'status.online': true });
  },
  usersOnlineCount() {
    return Meteor.users.find({ 'status.online': true }).count();
  },
  roleUsersOnline(name) {
    return Meteor.users.find({
      'status.online': true,
      'profile.role.specialty': name,
    });
  },
  languageUsersOnline(language) {
    return Meteor.users.find({
      'status.online': true,
      'profile.country.name': language,
    });
  },
  roleLanguageUsersOnline(name, language) {
    return Meteor.users.find({
      'status.online': true,
      'profile.country.name': language,
      'profile.role.specialty': name,
    });
  },
});

Template.status.events({
  'submit .submit-search': function (event) {
    event.preventDefault();

    const target = event.target;
    const language = target.language.value;
    const specialty = target.specialty.value;
    // console.log(Meteor.findOne(X));
  },
});
