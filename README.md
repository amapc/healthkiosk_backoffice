# README #

### What is this repository for? ###

* Backoffice for Health Kiosks
* Version 0.0.1

### How do I get set up? ###

1. Install latest LTS node.js
	https://nodejs.org/en/

2. Install meteor.js
	https://www.meteor.com/install

3. Install dependencies with `$ npm install`

4. Run application `$ meteor`

